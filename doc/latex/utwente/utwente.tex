\documentclass[11pt,a4paper,english]{article}

\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[ocgcolorlinks]{hyperref}
\usepackage[dutch,style]{utwentetitle}
\usepackage{babel,listings,pgffor,xltxtra,fancyhdr,parskip,multicol,enumitem}

\newfontfamily\tgheroscn
	[ Ligatures      = TeX ,
	  Extension      = .otf ,
	  UprightFont    = *-regular ,
	  ItalicFont     = *-italic ,
	  BoldFont       = *-bold ,
	  BoldItalicFont = *-bolditalic ]
	{texgyreheroscn}

\lstset{
	language=[LaTeX]TeX,
	basicstyle=\ttfamily,
	numbers=left,
	numberstyle=\tiny\color{gray}\ttfamily,
	tabsize=4,
	keywordstyle=\color{Blue},
	breaklines=true,
	morekeywords={subtitle,course,faculty,maketitle,supervisor},
	xleftmargin=.5cm,
	xrightmargin=.5cm
}

\hypersetup{
    colorlinks,
    citecolor=Goldenrod!80!Black,
    filecolor=Cerulean,
    linkcolor=BrickRed,
    urlcolor=Blue
}

\title{utwente}
\subtitle{University of Twente packages}
\author{Silke Hofstra}

\pagestyle{fancy}
\fancyhf{}
\lhead{\universtitle \uppercase{utwente}}
\rhead{\universtitle \leftmark}
\cfoot{\thepage}
\setcounter{tocdepth}{2}

\setlist[itemize]{label={},leftmargin=0pt}

\DeclareDocumentCommand\lopt{m}{\texttt{\bfseries#1} --}
\DeclareDocumentCommand\lcmd{m}{\lopt{\textbackslash#1}}
\DeclareDocumentCommand\cmd{m}{\texttt{\textbackslash#1}}

\begin{document}
\thispagestyle{plain}
\maketitle
This document describes the use of the collection of utwente packages.
These package that provides titles and style more in line with
the University of Twente (UT) house style.

\tableofcontents
{\let\thefootnote\relax\footnotetext{%
	\textbf{Copyright notice:}
	All images provided with this package are copyrighted by the University of Twente.
	They have been obtained from the University of Twente website
	(\url{http://www.utwente.nl/huisstijl}).
	The \LaTeX\ code from this package is distributed under the
	\href{http://www.latex-project.org/lppl/}{\LaTeX\ project public license} licence 
	(\url{http://www.latex-project.org/lppl/}).
}}

\clearpage
\section{Introduction}
This package is the result of an attempt to create a nicer looking 
\cmd{maketitle} command for work on the University of Twente.
It has since grown into a collection of fully functional \LaTeX\ packages.

\subsection{House Style}
\label{sub:housestyle}
While the titles from this package are certainly more in line with the UT house style, I chose to deviate on some points. The first is page layout. The provided guidelines are as follows:

\begin{quote}
	The corporate and faculty material has a horizontal layout.
	Teaching and research have a vertical layout.
	In these last two categories, a vertical axis is drawn from the top down.
	This axis forms the boundary for the use of elements and word trademark.
	The typography is always positioned to the right of this axis.
\end{quote}

This package tries to follow this guideline,
but the most important thing you have to do yourself:

\begin{quote}
	The University of Twente’s house style incorporates a range of different page layouts.
	These depend on the level in the brand architecture.
	Every publication has an airy and spacious arrangement.
	The pages are characterized by not being filled with text.
\end{quote}

To help style our document, an option is available (\texttt{style}, see~\ref{sec:opt}).
This option changes the typeface (see~\ref{sec:typeface}) as well.
This document was created by using this option.

\subsection{Typeface}
\label{sec:typeface}
A challenge when creating this package was trying to use the same typeface
as the official University of Twente logo.

The official font family (according to their website) is \emph{Linotype Univers}
(except for the full stop, which is round instead of square).
To me the logo looks like \emph{Linotype Univers 520 Condensed Medium}.
As Univers is not available in the most common \LaTeX\ packages
\footnote{See \url{http://www.tug.dk/FontCatalogue}}
this package resorts to either the OpenType version provided together
with this package or an alternative font.

What follows is a comparison between two alternative fonts \emph{Roboto}
and \emph{\TeX\ Gyre Heros}, and the official wordmark.
In figure~\ref{fig:logo} a couple of fonts are shown:
Linotype Univers, Roboto Condensed and \TeX\ Gyre Heros Condensed.
The first is the official wordmark.

\begin{figure}[h]
	\def\utu{\uppercase{Universiteit}}
	\def\utt{\uppercase{Twente}{\robotocondensed\addfontfeature{Scale=3.2}.}}
	\vspace{-6mm}
	\hspace{-8mm}\includegraphics[width=.9\textwidth]{utwente-nl-black}\vspace{-7mm}\\
	\textsc{\universtitle\addfontfeature{Scale=3.26}\utu\hspace{.3em}\utt}\\[1ex]
	\textsc{\robotocondensed\addfontfeature{Scale=3.24}\ \utu\ \utt}\\[1ex]
	\textsc{\tgheroscn\addfontfeature{Scale=3.20}\ \utu\ \utt}\\[1ex]
	
	\caption{Fonts compared -- Linotype Univers (official),
		Linotype Univers (from the package),
		Roboto Condensed and \TeX\ Gyre Heros Condensed}
	\label{fig:logo}
\end{figure}

This package tries to load \emph{Linotype Univers} by default when using \XeLaTeX\ or \LuaLaTeX.
An option (\texttt{freefont}) is available to use Roboto instead.
When using pdf\LaTeX\ Roboto (Condensed) is used for the title and default style.

More information on using these fonts can be found on the Font Catalogue page of Roboto\footnote{See \url{http://www.tug.dk/FontCatalogue/roboto}} and \TeX\ Gyre Heros\footnote{See \url{http://www.tug.dk/FontCatalogue/tgheros}}.

\clearpage
\section{Usage}

\begin{multicols}{2}

\subsection{utwentetitle}
\subsubsection{Options}
\label{sec:opt}
The package can be configured with the following options:
\begin{itemize}
	\item \lopt{titlepage} 
		creates a title page instead of a small title.
		This can also be set in your \cmd{documentclass} options.
	\item \lopt{style} 
		 Change the formatting of your document. See section~\ref{sub:housestyle}.
	
	\item \lopt{language = [en,nl]} 
		 set the language of the University of Twente wordmark.
	\item \lopt{english} 
		 use the English logo.
		This can also be set by setting the language in your class options.
	\item \lopt{dutch} 
		 use the Dutch logo, this is default.
		This can also be set by setting the language in your class options.
	
	\item \lopt{image = [number]} 
		 choose the banner image for the title page.
		Several pictures are available (see~\ref{sec:ov}).
		Custom pictures can be used by naming them in the format:
		\texttt{utwentetitle-back-[number]}
	
	\item \lopt{imageheight=[length]}  change the image height.
	\item \lopt{imagebannerwidth}  change the width of the image to the maximum banner width.
	\item \lopt{imageoffset=[length]}  change the vertical offset of the image.
	\item \lopt{imageopts=[options]} 
		 change the options of the image. Example: \texttt{imageopts=\{height=600pt\}}
	
	\item \lopt{type1}  force use of Type 1 (pdf\LaTeX) fonts.
	\item \lopt{otf}  force use of OpenType fonts, only for \XeLaTeX\ and \LuaLaTeX.
	\item \lopt{math}  also replace maths fonts, only for \XeLaTeX\ and \LuaLaTeX.
\end{itemize}

\subsubsection{Commands}
The packages also provides, modifies or uses the following commands:
\begin{itemize}
	\item \lcmd{date}  set the date, reverts to the current date when omitted.
	\item \lcmd{title}  set the title.
	\item \lcmd{subtitle}  set the subtitle.
	\item \lcmd{author}  set the author(s).
	\item \lcmd{supervisor}  set the supervisor(s). Only visible on the title page.
	\item \lcmd{course}  set the course.
	\item \lcmd{faculty}  set the faculty.
	\item \lcmd{maketitle}  create the title.
\end{itemize}

\subsubsection{Overrides}
Options can also be overridden with the following commands:
\begin{itemize}
	\item \lcmd{titlelanguage} 
		 override the \texttt{language} option.
	\item \lcmd{titleimage} 
		 override the title banner image (\texttt{image}).
	\item \lcmd{titleimageoffset} 
		 override the \texttt{imageoffset} option.
	\item \lcmd{titleimageopts} 
		 override the \texttt{imageopts} option.
\end{itemize}

%\subsubsection{Tips}
%
%\begin{lstlisting}
%\supervisor{%
%	\MakeUppercase{\fontsize{8}{10}\selectfont examencommissie}\\
%	<Commissieleden>\\[32.5mm]
%	\MakeUppercase{\fontsize{8}{10}\selectfont documentnummer}\\
%	\MakeUppercase{\fontsize{10}{10}\selectfont <AFD}\\
%	}
%\end{lstlisting}

\clearpage
\subsection{utwentefont}
\subsubsection{Options}
\label{sec:fontopt}
The package can be configured with the following options:
\begin{itemize}
	\item \lopt{default} 
		 set sans-serif as the default font family.
	\item \lopt{freefont} 
		 use Roboto, the free alternative to Univers.
	\item \lopt{otf, opentype} 
		 force use of OpenType fonts.
	\item \lopt{type1, t1} 
		 force use of Type1 fonts.
	\item \lopt{math} 
		 replace the maths font.
	\item \lopt{nomath} 
		 do not replace the maths font.
\end{itemize}

\subsubsection{Commands}
The packages also provides, modifies or uses the following commands:

\begin{itemize}
	\item \lcmd{rounddot} produces an expensive round dot.
	\item \lcmd{roboto} the Roboto font family.
	\item \lcmd{robotocondensed} the Roboto condesed font family
	\item \lcmd{univers} the Univers font family.
	\item \lcmd{universcn} the Univers Condensed font family.
	\item \lcmd{universtitle} the Univers variant used in the University of Twente wordmark.
\end{itemize}

\clearpage
\subsection{utwente-beamer}

\subsubsection{Options}
\label{sec:fontopt}
In addition to the option provided by \href{http://mirrors.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf}{the beamer class}, the class can be configured with the following options:

\begin{itemize}
	% Styling options
	\item \lopt{background = [number]}
		set the background of the title slide.
		Valid are numbers 1 to 4.
		Custom pictures can be used by naming them in the format:
		\texttt{utwente-beamer-back-[en,nl]-[number]}
	\item \lopt{banner = [number]}
		set the background of the title slide.
		Valid are numbers 1 to 5.
		Custom pictures can be used by naming them in the format:
		\texttt{utwente-beamer-banner-[number]}
	\item \lopt{wide}
		set the aspect ratio to 16:9 (widescreen).
		This is equivalent to \lopt{aspectratio = 169}.
	
	% Language options
	\item \lopt{language = [en,nl]} 
		set the language of the University of Twente wordmark.
	\item \lopt{english} 
		use the English logo.
		This can also be set by setting the language in your class options.
	\item \lopt{dutch} 
		use the Dutch logo, this is default.
		This can also be set by setting the language in your class options.

		
	% Options from utwentefont
	\item \lopt{freefont} 
		 use Roboto, the free alternative to Univers.
	\item \lopt{otf, opentype} 
		 force use of OpenType fonts.
	\item \lopt{type1, t1} 
		 force use of Type1 fonts.
	\item \lopt{math} 
		 replace the maths font.
\end{itemize}

\subsubsection{Commands}
The packages also provides, modifies or uses the following commands:

\begin{itemize}
	\item \lcmd{language} changes the language.
	\item \lcmd{background} changes the background.
	\item \lcmd{banner} changes the banner.
	\item \lcmd{maketitleslide} produces the title slide.
	\item \lcmd{contentsslide} produces a table of contents slide.
	\item \lcmd{setbanner} resets the banner.
	\item \lcmd{clearbanner} clears the banner.
\end{itemize}
\clearpage

\end{multicols}

\clearpage
\section{Installation}

If you've received the package in the form of a compressed archive (\texttt{.zip, .7z, .rar, .tar.gz}) you have to manually extract the archive to a location (probably a folder named \texttt{texmf}).
This location differs per distribution and operating system.

This directory structure should be placed directly inside your \texttt{texmf} (or equivalent) folder.

\subsection{Windows}
\subsubsection{MiK\TeX}
In MiK\TeX\ you might have to add a personal folder. This can either contain the package directly or be a folder for all your own packages. To edit the list open \texttt{MiKTeX > Maintenance > Settings} from your start menu.

\subsubsection{\TeX\ Live}
To find the folder in which you should place the package, press \texttt{Win+R} and run \texttt{cmd}. In the command prompt type: \texttt{kpsewhich -var-value=TEXMFHOME}. Place the package inside of the \texttt{tex} directory in the resulting folder (both directories might have to be created).

\subsection{Linux (\TeX\ Live)}
Installation in Linux systems (tested only on Ubuntu) is fairly straight-forward. Your personal packages are probably installed in the \texttt{texmf} directory in your home directory (\texttt{\textasciitilde/texmf}). Extracting the archive contents to a subfolder named \texttt{tex} will probably make the package work instantly. If one of the directories doesn't exist you might have to create it.
The location of the \texttt{texmf} folder can also be found by executing \texttt{kpsewhich -var-value=TEXMFHOME} in a terminal. This displays the location of your personal \texttt{texmf} folder. If you have \texttt{sudo} powers you can also edit the config file by running \texttt{sudo gedit \`{}kpsewhich texmf.cnf\`} and finding the \texttt{TEXMFHOME} line. Some like to change it to \texttt{\$HOME/.texmf} so the directory is hidden.

\subsection{Mac OSX (Mac\TeX)}
The package can be placed in the \texttt{tex} folder inside the \texttt{texmf} folder in your library (which can be opened in finder by option-clicking on the `go' menu). 
If the directory doesn't exist it might have to be created.

\subsection{Location Overview}
\begin{figure}[h]
	\centering
	\begin{tabular}{|l|c|l|}
		\hline
		Windows & \TeX\ Live & \texttt{C:\textbackslash users\textbackslash [username]\textbackslash texmf\textbackslash} \\
		        & MiK\TeX & \texttt{Start > MiKTeX > Maintenance > Settings}\\ \hline
		Linux & \TeX\ Live & \texttt{\textasciitilde/texmf/} \\ \hline
		Mac OSX & Mac\TeX & \texttt{\textasciitilde/Library/texmf/} \\ \hline
	\end{tabular}
\end{figure}

\clearpage
\section{Examples}
Following are some examples of how titles look when creating them with the utwentetitle package.
\subsection{Background overview}
\label{sec:ov}
The following background images are available for use in the package:

\setcounter{figure}{0}
\begin{figure}[h]
\centering
\foreach \n in {1,...,9}{
	\begin{minipage}[t]{0.18\textwidth}
		\centering
		\vspace{1ex}
		\n\\
		\fbox{\includegraphics[height=.24\paperheight]{utwentetitle-back-\n}}
		%\caption{}
	\end{minipage}%
}
\end{figure}


\subsection{Simple title}
This is the default replacement for \cmd{maketitle}, and is fairly simple. It is meant for things that don't require a title page.
\subsubsection{\LaTeX}
\begin{lstlisting}
\documentclass[english]{article}
\usepackage{utwentetitle}

\title{Title}
\subtitle{Subtitle}
\author{Author}
\course{Course}
\date{First of August, 2012}
\faculty{Faculty of Faculties}

\begin{document}
\maketitle
\end{document}
\end{lstlisting}

\subsubsection{Result}
\title{Title}
\subtitle{Subtitle}
\author{Author}
\course{Course}
\date{First of August, 2012}
\faculty{Faculty of Faculties}
\titlelanguage{en}
\begin{minipage}{\textwidth}
\maketitle
\end{minipage}

\clearpage
\subsection{Title Page 1}
The title page offers more information and more customisation options, it is a whole page after all.
\subsubsection{\LaTeX}
\begin{lstlisting}
\documentclass[english,titlepage]{article}
\usepackage[image=4]{utwentetitle}

\title{Title}
\subtitle{Subtitle}
\author{%
	\emph{By:}\\
	Author 1 -- s1234567\\
	Author 2 -- s1234567\\
	Author 3 -- s1234567\\
	Author 4 -- s1234567}
\supervisor{%
	\emph{Supervised by:}\\
	Supervisor}
\course{Course}
\faculty{Faculty of Faculties}

\begin{document}
\maketitle
\end{document}
\end{lstlisting}
\subsubsection{Result}
The next page is the result of the code above.
\author{%
	\emph{By:}\\
	Author 1 -- s1234567\\
	Author 2 -- s1234567\\
	Author 3 -- s1234567\\
	Author 4 -- s1234567}
\supervisor{%
	\emph{Supervised by:}\\
	Supervisor}
\date{\today}
\ExplSyntaxOn
\bool_set_true:N\utwentetitle_page
\ExplSyntaxOff
\titleimage{4}
\newpage
\maketitle

\clearpage
\subsection{Title Page 2}
The title page offers more information and more customisation options, it is a whole page after all.
\subsubsection{\LaTeX}
\begin{lstlisting}
\documentclass[english,titlepage]{article}
\usepackage[style,image=7,imagebannerwidth,imageoffset=-9cm,whitelogo]{utwentetitle}

\title{Title}
\subtitle{Subtitle}
\author{%
	\emph{By:}\\
	Author 1 -- s1234567\\
	Author 2 -- s1234567\\
	Author 3 -- s1234567\\
	Author 4 -- s1234567}
\supervisor{%
	\emph{Supervised by:}\\
	Supervisor}
\course{Course}
\faculty{Faculty of Faculties}

\begin{document}
\maketitle
\end{document}
\end{lstlisting}
\subsubsection{Result}
The next page is the result of the code above.
\author{%
	\emph{By:}\\
	Author 1 -- s1234567\\
	Author 2 -- s1234567\\
	Author 3 -- s1234567\\
	Author 4 -- s1234567}
\supervisor{%
	\emph{Supervised by:}\\
	Supervisor}
\date{\today}
\ExplSyntaxOn
\bool_set_true:N\utwentetitle_page
\tl_set:Nn\utwentetitle_logocolour{white}
\ExplSyntaxOff
\titleimage{7}
\titleimageopts{height=!,width=\bannerwidth}
\titleimageoffset{-9cm}
\newpage
\maketitle

\end{document}